<?php

/**
 * Front Controller for my Application
 */

// 1. Bootstrap the Application

// Enable use of the session
session_start();

// Use Composer to manage requiring files
require('vendor/autoload.php');

// 2. Determine which page to load from the request

if (isset($_GET['page'])) {
    $pageToLoad = 'templates/pages/' . $_GET['page'] . '.php';
}
else {
    $pageToLoad = 'templates/pages/front.php';
}

// 3. Ennsure the page actually exists...
if (file_exists($pageToLoad) === false) {
    header("HTTP/1.0 404 Not Found");
    $pageToLoad = 'templates/includes/404.php';
}


// 4. Load that page with common header and footer

require('templates/includes/header.php');
require($pageToLoad);
require('templates/includes/footer.php');
