<?php

$customerMapper = new \TicketOrdering\ModelMapper\CustomerMapper();
$rows = $customerMapper->getCustomers();

?>

<h1>Customers</h1>

<p>
    <?php 
        $howOldAmI = \Carbon\Carbon::createFromDate(1975, 5, 21)->age;
        echo $howOldAmI;
    ?>
</p>

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($rows as $row) {
            echo "<tr>";
            echo "<td>{$row->getName()}</td>";
            echo "<td>{$row->getEmailAddress()}</td>";
            echo "<td>{$row->getPhoneNumber()}</td>";
            echo "</tr>";
        }
        ?>
    </tbody>
</table>