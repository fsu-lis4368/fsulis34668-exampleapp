<?php

    if (count($_POST) > 0) {
        
        $year  = $_POST['year'];
        $month = $_POST['month'];
        $day   = $_POST['day'];

        $howOld = \Carbon\Carbon::createFromDate($year, $month, $day)->age;
        
        echo "<strong>You are " . $howOld . " years old</strong>";
    }

?>

<h1>Age Calculation Form</h1>

<form method="post">
    <label>Year</label>
    <input type='number' name='year' />
    <label>Month</label>
    <input type='number' name='month' />
    <label>Day</label>
    <input type='number' name='day' />
    
    <button type='submit'>Get My Age</button>
</form>
