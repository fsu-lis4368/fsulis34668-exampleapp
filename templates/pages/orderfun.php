<h1>Ticket System Ordering Fun</h1>

<?php

// This is a customer
$customer = new \TicketOrdering\Model\Customer('Bob Smith', 'bob@example.org');

// These are seats
$seats = [
    new TicketOrdering\Model\Seat('A105'),
    new TicketOrdering\Model\Seat('B102'),
    new TicketOrdering\Model\Seat('C103')
];

// Here is my order
$order = new TicketOrdering\Model\Order('12345', $customer, $seats);
$ipOrder = new TicketOrdering\Model\InPersonOrder('555', $customer);
$olOrder = new TicketOrdering\Model\OnlineOrder('666', $customer);


echo "<p>" . $order->getOrderNumber() . "</p>";
echo "<p>" . $order->getCustomer()->getName() . "</p>";

foreach ($order->getSeats() as $seat) {
    echo "<li>" . $seat->getNumber() . "</li>";
}



?>