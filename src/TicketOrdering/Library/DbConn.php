<?php

namespace TicketOrdering\Library;

class DbConn
{
    /**
     * Get a database connection
     * 
     * @return \PDO
     */
    public function getDatabase()
    {
        $user = 'root';
        $pass = 'root';
        $host = '192.168.40.40';
        $dbName = 'lis4368';

        return new \PDO("mysql:host=$host;dbname=$dbName", $user, $pass);
    }
}