<?php

namespace TicketOrdering\Model;

class Customer
{
    /**
     * @var string 
     */
    protected $name;

    /**
     * @var string 
     */
    protected $emailAddress;

    /**
     * @var string 
     */
    protected $phoneNumber;
    
    // ---------------------------------------

    /**
     *  Constructor
     * 
     * @param string $name
     * @param string $emailAddress
     * @param string $phoneNumber
     */
    public function __construct($name, $emailAddress, $phoneNumber = '')
    {        
        $this->setName($name);
        $this->setEmailAddress($emailAddress);
        $this->setPhoneNumber($phoneNumber);
    }
    
    // ---------------------------------------
    
    /**
     * Get the name
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get email address
     * 
     * @return string
     */
    public function getEmailAddress() {
        return $this->emailAddress;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setEmailAddress($emailAddress) {
        $this->emailAddress = strtolower($emailAddress);
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }
    
}
