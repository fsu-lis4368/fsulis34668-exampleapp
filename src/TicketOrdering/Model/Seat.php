<?php

namespace TicketOrdering\Model;

class Seat 
{
    // Seat number / is immutable
    protected $number;
    
    // order that is associated with this seat
    protected $order;
    
    // --------------------
    
    public function __construct($number)
    {
        $this->number = $number;
    }
 
    public function getOrder() {
        return $this->order;
    }

    public function setOrder($order) {
        $this->order = $order;
    }
    
    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    
}
