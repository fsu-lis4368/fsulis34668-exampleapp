<?php

namespace TicketOrdering\Model;

use DateTime;

/**
 * Abstract Order Class
 * 
 * Cannot be instantiated directly
 */
abstract class Order
{
    // Order number
    protected $orderNumber;

    // Created timestamp
    protected $created;

    // created
    // completed
    // processed
    // shipped    
    protected $status;
    
    /**
     * @var array|Seat[]
     */
    protected $seats;
    
    /**
     * @var Customer
     */
    protected $customer;
    
    /**
     * Order Constructor
     * 
     * @param string $number      The Order Id; should be unique
     * @param Customer $customer  The customer who is creating this order
     * @param array|Seat[] $seats The Seats the Customer Ordered
     */
    public function __construct($number, Customer $customer, array $seats = []) 
    {
        $this->setStatus('created');
        $this->setOrderNumber($number);
        $this->setCustomer($customer);
        $this->setSeats($seats);
        
        $this->created = new DateTime();        
    }
    
    /**
     * Get the order number
     * 
     * @return string
     */
    public function getOrderNumber() {
        return $this->orderNumber;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * Get the seats
     * @return array|Seat[]
     */
    public function getSeats()
    {
        return $this->seats;
    }
    
    /**
     * Set seats
     * @param array $seats
     */
    public function setSeats(array $seats)
    {
        $this->seats = $seats;
    }
    
    public function setOrderNumber($orderNumber) {
        $this->orderNumber = $orderNumber;
    }

    public function setCreated($created) {
        $this->created = $created;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getCustomer() {
        return $this->customer;
    }

    public function setCustomer(Customer $customer) {
        $this->customer = $customer;
    }


    
}