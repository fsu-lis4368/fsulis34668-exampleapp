<?php

namespace TicketOrdering\Model;

/**
 * Online Order Class
 */
class OnlineOrder extends Order
{
    /**
     * @var string
     */
    protected $customerIpAddress;
    
    
    public function __construct($number, Customer $customer, mixed $seats = array()) {
        parent::__construct($number, $customer, $seats);
        
        // Set the IP Address automatically
        $this->customerIpAddress = $_SERVER['REMOTE_ADDR'];
    }
    
    /**
     * @return string
     */
    public function getCustomerIpAddress()
    {
        return $this->customerIpAddress;
    }

}
