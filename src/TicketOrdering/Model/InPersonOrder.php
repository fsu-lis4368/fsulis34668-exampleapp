<?php

namespace TicketOrdering\Model;

/**
 * Order Placed In-Person
 */
class InPersonOrder extends Order
{
    /**    
     * @var string
     */
    protected $whoSoldTheTicket;
    
    /**
     * @return string
     */
    function getWhoSoldTheTicket() {
        return $this->whoSoldTheTicket;
    }

    /**
     * @param string $whoSoldTheTicket  The employee ID of who sold the ticket
     */
    function setWhoSoldTheTicket($whoSoldTheTicket) {
        $this->whoSoldTheTicket = $whoSoldTheTicket;
    }


}