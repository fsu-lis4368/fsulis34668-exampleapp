<?php

namespace TicketOrdering\ModelMapper;

use TicketOrdering\Library\DbConn;
use TicketOrdering\Model\Customer;

/**
 * Description of CustomerMapper
 *
 * @author casey
 */
class CustomerMapper 
{
    /**
     * Get customers from the database.
     * 
     * @return Customer[]  A list of Customer Objects
     */
    public function getCustomers()
    {
        $db = new DbConn();
        $dbConn = $db->getDatabase();

        $stmt = $dbConn->prepare("SELECT * FROM customers");
        $stmt->execute();        
       
        $outArray = array();
        
        // $stmt->fetch() always gets the NEXT row every time it is called..
        while ($row = $stmt->fetch()) {
            $outArray[] = new Customer($row['name'], $row['email'], $row['phoneNumber']);
        }
   
        return $outArray;
    }
}
